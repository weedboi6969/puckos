#include <string.h>

char* strcpy(char* dest, const char* src) {
	uint16_t len;
	if(strlen(src) >= strlen(dest)) len = strlen(dest);
	else len = strlen(src);
	for(uint16_t i = 0; i < len; i++) dest[i] = src[i];
	return dest;
}