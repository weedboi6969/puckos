#include <stdlib.h>
#include <kernel/tty.h>

int atoi(const char *str) {
	if(*str == NULL) return 0;
	uint32_t i = 0;
	int sign = 1, res = 0;
	if(str[0] == '-') {
		sign = -1;
		i++;
	}
	for(; str[i] != 0; i++) {
		if((str[i] < '0') || (str[i] > '9')) {
			tty_puts("[ERROR] atoi(): non-numerical character encountered, returning 0");
			return 0;
		}
		res = (res * 10) + (str[i] - '0');
	}
	return sign * res;
}