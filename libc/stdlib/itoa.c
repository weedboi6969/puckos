#include <stdlib.h>

static void reverse(char str[], uint32_t length) {
	uint32_t start = 0, end = length - 1;
	while(start < end) {
		char t = *(str + start);
		*(str + start) = *(str + end);
		*(str + end) = t;
		start++;
		end--;
	}
}

char* itoa(int num, char* str, uint8_t base) {
	uint32_t i = 0;
	uint8_t is_neg = 0;
	
	if(num == 0) {
		str[i++] = '0';
		str[i] = '\0';
		return str;
	}
	
	if(num < 0 && base == 10) {
		is_neg = 1;
		num = -num;
	}
	
	while(num != 0) {
		uint32_t rem = num % base;
		str[i++] = (rem > 9) ? ((rem - 10) + 'a') : (rem + '0');
		num /= base;
	}
	
	if(is_neg) str[i++] = '-';
	str[i] = 0;
	
	reverse(str, i);
	return str;
}

char* itoa_unsigned(uint32_t num, char* str, uint8_t base) {
	uint32_t i = 0;
	
	if(num == 0) {
		str[i++] = '0';
		str[i] = '\0';
		return str;
	}
	
	while(num != 0) {
		uint32_t rem = num % base;
		str[i++] = (rem > 9) ? ((rem - 10) + 'a') : (rem + '0');
		num /= base;
	}

	str[i] = 0;
	
	reverse(str, i);
	return str;
}