#include <stdio.h>

#if defined(__is_libk)
#include <kernel/tty.h>
#endif

int puts(const char *string) {
#if defined(__is_libk)
	tty_puts(string);
#else
	// TODO: implement proper stdio and write system call
#endif
	return 0;
}