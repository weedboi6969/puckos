#include <stdio.h>

#if defined(__is_libk)
#include <kernel/tty.h>
#endif

int putchar(int ic) {
#if defined(__is_libk)
	tty_putc((uint8_t) ic);
#else
	// TODO: implement proper stdio and write system call
#endif
	return ic;
}