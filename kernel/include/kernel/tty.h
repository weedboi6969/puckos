/*
	tty.h - TTY emulator piggybacking from video.c
	<C> Copyright 2018 Weedboi6969@RTEA
*/

#ifndef KERNEL_TTY_H
#define KERNEL_TTY_H

#include <stddef.h>
#include <stdint.h>

void tty_setbg(uint32_t color);
void tty_setfg(uint32_t color);
void tty_init(void);
void tty_clear(void);
void tty_setxy(uint16_t x, uint16_t y);
void tty_getxy(uint16_t *x, uint16_t *y);
void tty_putc(uint8_t c);
void tty_puts(const char *s);

#endif