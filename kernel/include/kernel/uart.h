/* 
	uart.h - UART/COM library
	Copyright 2018 Weedboi6969@Real Time Engineering Associates.
	This file contains function declarations needed for UART/COM
	communication and therefore cross-platform. You can include
	this in your files in your platform as long as your platform's
	arch directory contains uart.c.
*/

#ifndef KERNEL_UART_H
#define KERNEL_UART_H

#include <stddef.h>
#include <stdint.h>

void uart_init(uint8_t port, uint32_t baud);
uint8_t uart_received(uint8_t port);
uint8_t uart_getc(uint8_t port);
uint8_t uart_empty(uint8_t port);
void uart_putc(uint8_t port, uint8_t c);
void uart_puts(uint8_t port, const char *s);

#endif