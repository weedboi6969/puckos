.global idt32_dummy

idt32_dummy:
iret

.global exception_DE
.global exception_DB
.global exception_NMI
.global exception_BP
.global exception_OF
.global exception_BR
.global exception_UD
.global exception_NM
.global exception_DF
.global exception_CSO
.global exception_TS
.global exception_NP
.global exception_SS
.global exception_GP
.global exception_PF
.global exception_MF
.global exception_AC
.global exception_MC
.global exception_XF
.global exception_VE
.global exception_SX
.extern exception_handler

exception_stub:
pusha
mov %ds, %ax
push %eax
mov $0x10, %ax
mov %ax, %ds
mov %ax, %es
mov %ax, %fs
mov %ax, %gs
mov %ax, %ss

call exception_handler

pop %eax
mov %ax, %ds
mov %ax, %es
mov %ax, %fs
mov %ax, %gs
mov %ax, %ss
popa
add $8, %esp
iret

exception_DE:
cli
push $0x00
push $0x00
jmp  exception_stub

exception_DB:
cli
push $0x00
push $0x01
jmp exception_stub

exception_NMI:
cli
push $0x00
push $0x02
jmp exception_stub

exception_BP:
cli
push $0x00
push $0x03
jmp exception_stub

exception_OF:
cli
push $0x00
push $0x04
jmp exception_stub

exception_BR:
cli
push $0x00
push $0x05
jmp exception_stub

exception_UD:
cli
push $0x00
push $0x06
jmp exception_stub

exception_NM:
cli
push $0x00
push $0x07
jmp exception_stub

exception_DF:
cli
push $0x08
jmp exception_stub

exception_CSO:
cli
push $0x00
push $0x09
jmp exception_stub

exception_TS:
cli
push $0x0A
jmp exception_stub

exception_NP:
cli
push $0x0B
jmp exception_stub

exception_SS:
cli
push $0x0C
jmp exception_stub

exception_GP:
cli
push $0x0D
jmp exception_stub

exception_PF:
cli
push $0x0E
jmp exception_stub

exception_MF:
cli
push $0x00
push $0x10
jmp exception_stub

exception_AC:
cli
push $0x11
jmp exception_stub

exception_MC:
cli
push $0x00
push $0x12
jmp exception_stub

exception_XF:
cli
push $0x00
push $0x13
jmp exception_stub

exception_VE:
cli
push $0x00
push $0x14
jmp exception_stub

exception_SX:
cli
push $0x00
push $0x1E
jmp exception_stub
