/*
	video.c - Generic VBE driver
	<C> Copyright 2018 Weedboi6969@RTEA
	This file was made using these documentation(s):
	https://wiki.osdev.org/User:Omarrx024/VESA_Tutorial
	https://github.com/szhou42/osdev
	https://github.com/adafruit/Adafruit-GFX-Library
	https://wiki.osdev.org/Drawing_In_Protected_Mode
*/

#include <kernel/video.h>
#include <kernel/int32.h>
#include <kernel/paging86.h>
#include <math.h>
#include <string.h>

uint8_t *video_frame; // framebuffer
uint16_t x_offset = 0, y_offset = 0; // offset for character grid
uint32_t video_width, video_height;
uint8_t *video_font;

typedef struct __attribute__ ((packed)) {
	uint8_t signature[4]; // must be "VESA"
	uint16_t version;
	uint32_t oem; // segment:offset pointer to OEM name
	uint32_t capabilities;
	uint32_t video_modes; // segment:offset pointer to list of supported video modes
	uint16_t video_mem; // memory size in 64K blocks
	uint16_t oem_sw_rev;
	uint32_t oem_vendor; // segment:offset pointer to card vendor string
	uint32_t oem_prod_name; // segment:offset pointer to model name
	uint32_t oem_prod_rev; // segment:offset pointer to model revision
	uint8_t reserved[222];
	uint8_t oem_data[256];
} vbe_info_t;

typedef struct __attribute__ ((packed)) {
	uint16_t attributes;
	uint8_t window_a, window_b;
	uint16_t granularity;
	uint16_t window_size;
	uint16_t seg_a, seg_b;
	uint32_t win_func_ptr;
	uint16_t pitch;
	
	uint16_t res_x, res_y;
	uint8_t w_char, y_char, planes, bpp, banks;
	uint8_t mem_model, bank_size, img_pages;
	uint8_t reserved0;
	
	uint8_t red_mask, red_pos;
	uint8_t green_mask, green_pos;
	uint8_t blue_mask, blue_pos;
	uint8_t reserved_mask, reserved_pos;
	uint8_t directcolor_attribs;
	
	uint32_t physbase;
	uint32_t offscreenmem_off;
	uint16_t offscreenmem_size;
	uint8_t reserved1[206];
} vbe_mode_info_t;

vbe_info_t vbe_info;
vbe_mode_info_t vbe_mode_info;

#define SEGOFF_PTR(seg, off)			((seg << 4) | off)
#define VBE_PTR(arr)					SEGOFF_PTR(((arr & 0xFFFF0000) >> 16), (arr & 0xFFFF))

static uint16_t vbe_find_mode(uint32_t width, uint32_t height, uint8_t bpp) {
	uint16_t *modes = VBE_PTR(vbe_info.video_modes);
	uint32_t i = 0;
	while(modes[i] != 0xFFFF) {
		/* get mode info */
		vbe_mode_info_t *mode_info = 0x3000;
		int32_regs_t regs;
		regs.ax = 0x4F01;
		regs.cx = modes[i];
		regs.es = 0x0000;
		regs.di = 0x3000;
		int32(0x10, &regs);
		if(mode_info->res_x == width && mode_info->res_y == height && mode_info->bpp == bpp) return modes[i];
		i++;
	}
	return 0;
}

void video_putpixel(uint16_t x, uint16_t y, uint32_t color) {
	uint32_t where = y * vbe_mode_info.pitch + x * (vbe_mode_info.bpp / 8);
	video_frame[where] = color & 255; // blue
	video_frame[where + 1] = (color >> 8) & 255; // green
	video_frame[where + 2] = (color >> 16) & 255; // red
}

void video_fill(uint32_t color) {
	for(uint16_t y = 0; y < video_height; y++) {
		for(uint16_t x = 0; x < video_width; x++) {
			video_putpixel(x, y, color);
		}
	}
}

void video_scrollup(uint32_t lines, uint32_t bg) {
	for(uint32_t i = (lines * vbe_mode_info.pitch); i < (video_height * vbe_mode_info.pitch); i++) {
		video_frame[i - (lines * vbe_mode_info.pitch)] = video_frame[i];
	}
	for(uint32_t i = ((video_height - lines) * vbe_mode_info.pitch); i < (video_height * vbe_mode_info.pitch); i++) {
		video_frame[i] = bg;
	}
}

uint32_t rgb(uint8_t r, uint8_t g, uint8_t b) {
	return (b | (g << 8) | (r << 16));
}

int8_t video_init(uint16_t width, uint16_t height) {
	int32_regs_t regs; // for int32 operations
	
	/* get VBE info block */
	regs.ax = 0x4F00;
	regs.es = 0x0000;
	regs.di = 0x2000;
	int32(0x10, &regs);
	if((regs.ax & 0xFF) != 0x4F) return -1; // VBE not supported
	memcpy(&vbe_info, 0x2000, sizeof(vbe_info_t));
	
	uint16_t mode = vbe_find_mode(width, height, 32);
	if(!mode) return -2; // cannot find mode
	
	/* do the switch */
	regs.ax = 0x4F02;
	regs.bx = mode | 0x4000;
	int32(0x10, &regs);
	
	/* get framebuffer and mode information */
	regs.ax = 0x4F01;
	regs.cx = mode;
	regs.es = 0x0000;
	regs.di = 0x3000;
	int32(0x10, &regs);
	memcpy(&vbe_mode_info, 0x3000, sizeof(vbe_mode_info_t));
	video_frame = vbe_mode_info.physbase;
	
	video_width = width; video_height = height;
	paging86_map(video_frame, video_frame, (video_height * vbe_mode_info.pitch), 0, 1, current_directory);
	video_fill(0x00000000);
	return 0; // done
}

#define swap(a, b) {uint16_t t = a; a = b; b = t;}

void video_drawline(uint16_t x0, uint16_t y0, uint16_t x1, uint16_t y1, uint32_t color) {
	/* Bresenham's algorithm */
	uint8_t steep = abs(y1 - y0) > abs(x1 - x0);
	if(steep) {
		swap(x0, y0);
		swap(x1, y1);
	}
	
	if(x0 > x1) {
		swap(x0, x1);
		swap(y0, y1);
	}
	
	int16_t dx, dy;
	dx = x1 - x0;
	dy = abs(y1 - y0);
	
	int16_t err = dx / 2;
	int16_t ystep;
	
	if(y0 < y1) ystep = 1;
	else ystep = -1;
	
	for(; x0 <= x1; x0++) {
		if(steep) video_putpixel(y0, x0, color);
		else video_putpixel(x0, y0, color);
		err -= dy;
		if(err < 0) {
			y0 += ystep;
			err += dx;
		}
	}
}

void video_drawrect(uint16_t x0, uint16_t y0, uint16_t x1, uint16_t y1, uint32_t color) {
	video_drawline(x0, y0, x1, y0, color);
	video_drawline(x0, y1, x1, y1, color);
	video_drawline(x1, y0, x1, y1, color);
	video_drawline(x0, y0, x0, y1, color);	
}

void video_fillrect(uint16_t x0, uint16_t y0, uint16_t x1, uint16_t y1, uint32_t color) {
	if(y0 > y1) swap(y0, y1);
	for(uint16_t i = y0; i < y1; i++) video_drawline(x0, i, x1, i, color);
}

void video_drawbitmap(uint16_t x, uint16_t y, uint8_t *bitmap, uint16_t w, uint16_t h) {
	if(x + w >= video_width || y + h >= video_height) return; // sanity check
	
	for(uint16_t i = 0; i < h; i++) {
		for(uint16_t j = 0; j < w; j++) {
			video_putpixel(x + j, y + i, bitmap[i * w + j]);
		}
	}	
}

void video_setoffset(uint16_t x, uint16_t y) {
	x_offset = x; y_offset = y;
}

void video_getoffset(uint16_t *x, uint16_t *y) {
	*x = x_offset; *y = y_offset;
}

void video_putc(uint16_t x, uint16_t y, uint16_t c, uint8_t size, uint32_t color) {
	if(size == 0) return;
	x += x_offset;
	y += y_offset;
	if((x + (8 * size)) > video_width || (y + (8 * size)) > video_height) return;
	uint16_t x_old = x;
	for(uint8_t i = 0; i < 8; i++) {
		uint8_t t = video_font[c * 8 + i];
		for(uint8_t j = 0; j < 8; j++) {
			if(t & 1) video_drawrect(x, y, x + size - 1, y + size - 1, color);
			else video_drawrect(x, y, x + size - 1, y + size - 1, 0x00000000);
			t >>= 1;
			x += size;
		}
		x = x_old;
		y += size;
	}
}

void video_puts(uint16_t x, uint16_t y, const uint8_t *s, uint8_t size, uint32_t color) {
	for(uint32_t i = 0; s[i] != 0; i++) {
		if(s[i] == '\r') x = 0;
		else if(s[i] == '\n') {
			y++;
			if(y >= (video_height / 8)) {
				return;
			}
		} else {
			video_putc(x, y, s[i], size, color);
			x++;
			if(x >= (video_width / 8)) {
				x = 0;
				y++;
				if(y >= (video_height / 8)) {
					return;
				}
			}
		}
	}
}

void video_setfont(uint8_t *font) {
	video_font = font;
}